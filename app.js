$(document).ready(function(){

	Settings.load();
	App.setupViews();

	var blip = App.readyLoadService();
	if (blip) {
		App.startService(blip);
	} else {
		App.firstStart();
	}

});

var App = (function(){

	Platform.installOmnibox();

	function checkForDesktopUpdates() {
		if (window.Titanium) {
			/*
			var url = 'https://github.com/lukaszkorecki/Mikrob.chrome/raw/master',
			req = new Request(url);
			
			req.get('/manifest.json',{
				onSuccess : function(response) {
					var here = parseInt($('#preferences h1').html().replace(/[a-zA-z\.\s]/g, ''), 10),
					obj = JSON.parse(response.responseText),
					there = parseInt(obj.version.replace(/\./g,''),10);

					if(here < there) {
						alert('Jest nowa wersja desktopowego Mikroba! Odwiedź http://mikrob.koras.pl');
					}
				},
				onFailure : function() { }
			});			
			*/
		}
	}
	
	// legacy stuff clean up
	if (typeof localStorage.status_store !== 'undefined') {
		delete localStorage.status_store;
	}

	function firstStart() {
		Mikrob.Controller.hidePreferencesWindow();
		Mikrob.Controller.setUpLoginWindow();
		Mikrob.Controller.showLoginWindow();

	}
	function rescueOverQuota() {
		var prefs = localStorage.mikrob_preferences,
		pass = localStorage.access_token_secret,
		login = localStorage.access_token;

		localStorage.clear();

		localStorage.mikrob_preferences = prefs;
		localStorage.access_token_secret = pass;
		localStorage.access_token = login;
	}

	var statusStore = new CollectionStore('status_store', rescueOverQuota),
	shortlinkStore = new CollectionStore('shortlink', rescueOverQuota);

	function setupViews() {
		Mikrob.Controller.hideLoginWindow();
		Mikrob.Controller.hidePreferencesWindow();

		Mikrob.Controller.setUpOptionalStylesheet();
		Mikrob.Controller.setUpCharCounter();
		Mikrob.Controller.setUpBodyCreator();
		Mikrob.Controller.setUpLoginWindow();
		Mikrob.Controller.setUpPreferencesWindow();

		Mikrob.Controller.setupMoreForm();		
	}

	function setupViewports() {
		Mikrob.Controller.setUpViewports();
		Mikrob.Controller.setUpSidebars();
	}

	function readyLoadService() {
		if (localStorage.access_token && localStorage.access_token_secret) {
			Mikrob.User.storeCredentials(localStorage.access_token, localStorage.access_token_secret);
		}
		var user = Mikrob.User.getCredentials();
		var blip = false;

		if (user.access_token && user.access_token_secret) {
			localStorage.removeItem('password');

			Mikrob.Service.OAuthReq.setAccessTokens(user.access_token, user.access_token_secret);
			blip = new Blip((''), Mikrob.Service.OAuthReq);
		} else {
			Mikrob.Controller.showLoginWindow();
		}
		return blip;
	}

	function startService(blip) {
		if (blip) {
			Mikrob.Service.getCurrentUsername(blip,function(){
				this.setupViewports();
				Mikrob.Service.loadDashboard( function(){
					Mikrob.Controller.populateInboxColumns(function(){
						Mikrob.Controller.updateRelativeTime();
					});
				});
			}.bind(this));


			setInterval(function(){
				if(Settings.check.canPoll) {
					Mikrob.Service.updateDashboard();
				}
			}, Settings.check.refreshInterval);

			setInterval(Mikrob.Controller.updateRelativeTime, 30 * 1000);
		}
	}
	return {
		firstStart : firstStart,
		setupViews : setupViews,
		setupViewports: setupViewports,
		readyLoadService : readyLoadService,
		startService : startService,
		statusStore : statusStore,
		shortlinkStore : shortlinkStore,
		checkForDesktopUpdates : checkForDesktopUpdates
	};
})();

TESTHANDLERS = {
	onSuccess : function(r) {
		console.log('ok');
		console.dir(r);
	},
	onFailure : function(r) {
		console.log('fail');
		console.dir(r);
	}
};

var LoadTestData = function() {
	Settings.check.canPoll = false;
	var coll = [], idx = null;
	for(idx in localStorage) {
		if(idx.match(/^status_store_/)) {
			coll.push(JSON.parse(localStorage[idx]));
		}
	}
	App.setupViewports();
	Mikrob.Controller.renderDashboard(coll, true);
};
// Shims
if (!Function.prototype.bind) {
	Function.prototype.bind = function(scope) {
		var _function = this;
		return function() {
			return _function.apply(scope, arguments);
		};
	};
}

// Titanium workers are w3c
if (!Worker) {
	Worker = Titanium.Worker;
}

// disable httpClient so that Titanium Desktop doesn't leak
// also notify about updates!
if (typeof Titanium != 'undefined' && Titanium.Network) {
	Titanium.Network.createHTTPClient = undefined;
	App.checkForDesktopUpdates();
}