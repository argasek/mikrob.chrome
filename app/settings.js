// default settings if none are set
var Settings = (function(){
	// public
	var check = {};

	// private
	function defaults() {
		var def = {
			refreshInterval : 8000,
			notificationTimeout : 3000,
			notificationsEnabled : true,
			notifyDm : true,
			notifyPm : true,
			notifySt : true,
			messagesPerPage : 20,
			canPoll : true,
			lookStyle : "light.css",
			autoShortenLinks : true
		};

		localStorage.mikrob_preferences = JSON.stringify(def);
		return def;
	}

	// private
	function setFormData(data) {
		$('#preferences input').each(function(i, el) {
			// this can be changed to a regex later on
			// so that diff types are accepted
			if(el.type == 'hidden' || el.type == 'text') {
				$(el).attr('value', data[el.name]);
			} else if (el.type == 'checkbox') {
				if (data[el.name]) {
					$(el).attr('checked', "checked");
				}
			}
		});
		var selector = data.notificationsEnabled ? 0 : 1;
		$('#preferences input[type=radio]').dom[selector].checked = true;
		$('#preferences select').each(function(i, el) {
			$(el).val(data[el.name]);
		});
	}

	// private
	function getFormData() {
		var data = {};
		$('#preferences input').each(function(i, el) {
			switch(el.type) {
				case 'text':
				case 'hidden':
					data[el.name] = JSON.parse(el.value);
					break;
				case 'radio':
					// assume that if it's not enabled, then it's disabled
					var enabled = !! $('#preferences input[type=radio]').dom[0].checked;
					data[el.name] = enabled;
					break;
				case 'checkbox':
					data[el.name] = el.checked;
					break;
				default:
					break;
			}
		});

		$('#preferences select').each(function(i, el) {
			data[el.name] = el.value;
		});

		return data;
	}

	// public
	function load() {
		if (!!localStorage.mikrob_preferences) {
			this.check = JSON.parse(localStorage.mikrob_preferences);
		} else {
			this.check = defaults();
			localStorage.mikrob_preferences = JSON.stringify(this.check);
		}
		setFormData(this.check);
	}

	// public
	function save() {
		this.check = getFormData();
		localStorage.mikrob_preferences = JSON.stringify(this.check);
	}

	return {
		check : check,
		load : load,
		save : save
	};
})();
